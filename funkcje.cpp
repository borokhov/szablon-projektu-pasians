#include "stdafx.h"
#include "Karta.h"
#include <iostream>
#include <list>
#include <ctime>

using namespace std;

void CreateTalia (){
	
list<Karta> Talia;

for (int i=2; i!=15; i++){
	for (int j=1; j!=5; j++){
	Karta karta;
	karta.rank = i;
	karta.kolor = j;
	karta.k = 0;
	Talia.push_back(karta);
}
}
}

void WymieszajKarty (list<Karta> &Talia){
	int numer1, numer2;
	srand ((double)time(NULL));

	for (int i=0; i!=101; i++){
	numer1 = rand()%52;
	numer2 = rand()%52;
	void ZamienKarty(int numer1, int numer2);
}	
}

void ZamienKarty(int one, int two, list<Karta> &Talia){
	Karta tmp;
	int a = one;
	int b = two;
	 list<Karta>::iterator it1;	   //teraz mam przejsc do odpowiedniej karty w talii -> iterator
	 it1 = Talia.begin();
	 for (int i = 0; i <= a; i++)
		 it1++;
	
	tmp.rank = it1->rank;	   //kopiowanie z karty do tmp
	tmp.kolor = it1->kolor;
	list<Karta>::iterator it2;
		it2 = Talia.begin();
		for (int i = 0; i <= b; i++)
			it2++;
	it1->rank = it2->rank;
	it1->kolor = it2->kolor;
	it2->rank = tmp.rank;
	it2->kolor = tmp.kolor;
}

void WyswietlKarty(list<Karta> Talia){
	 list<Karta>::iterator it;
	for( it=Talia.begin(); it!=Talia.end(); ++it )
	   {
	   cout<<"====="; 
	   }
	   cout<<endl;
	   for( it=Talia.begin(); it!=Talia.end(); ++it )
	    {		
		 cout<<"|"<<it->rank<<"/"<<it->kolor<<"|";  
		 }
		 cout<<endl;
	    for( it=Talia.begin(); it!=Talia.end(); ++it )
		{
		 cout<<"=====";			   
		}
		cout<<endl;
}

int main()
{
	void CreateTalia();
	void WymieszajKarty(list<Karta> &Talia);
	void WyswietlKarty(list<Karta> Talia);

	return 0;
}